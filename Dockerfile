FROM node:20.12.1-alpine

COPY package.json . 
RUN npm i 
COPY . .
CMD [ "node", "index.js" ]